---
title: About Me
layout: posts
---

Hi, I'm Ryan Martin. I'm a computer science student at
[Asia Pasific University](https://apu.edu.my), Malaysia. My interests include
but not limited to FOSS, compilers, databases, cybersecurity, linux, pixel
graphic games. If you like this blog, you can subscribe with [this](../feed.xml).

Here are some links:
- [Email](mailto:ryan.mrtinn@gmail.com)
- [GitHub](https://github.com/rmrt1n)
- [LinkedIn](https://www.linkedin.com/in/ryan-martin-3b859b205/)

