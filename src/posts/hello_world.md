---
title: Hello World
date: "2021-06-13"
---

Hi There! 👋  
This is the start of my blog. This blog will contain things I find useful or 
interesting, some of my notes, and fixes or solutions to some bugs or 
problems I'll encounter. I won't be posting regularly (atleast for now), just 
when I have the urge to or when I've found something I want to share. Hopefully 
some of the things I write here will benefit someone else out there.

```c
puts("hello world!");
```

